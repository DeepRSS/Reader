 TODO list:
 * TODO Add scheduler (queue?)
 * TODO Add cronjob
 * TODO Rearrange reading and saving around async?
 * TODO Save hash of original feed response body and hash of original entry (benchmark?)
 * TODO Handle http vs. https uniqueness
 * add node.js, NPM and yarn to docker ...
 
 Ideas:
 * tags
 * time to read
 * text complexity
 * click-bait estimation
 * stories (similar articles, time proximity, source?)
 * follow ups (similar articles, novelty, long after)
 * twitter / reddit / hackernews / altmetrics / pocket
 * tracking with plugin
 
Installing Docker:

 ```bash
sudo apt install docker.io
```
* Add yourself to docker group
```bash
sudo usermod -a -G docker $USER
```
* after that a full system restart is required

Testing Gitlab-CI (Docker required):
 ```bash
sudo apt install gitlab-runner
``` 
* Edit /usr/lib/gitlab-runner/mk-prebuilt-images.sh according to: 
https://stackoverflow.com/questions/48891207/gitlab-ci-build-failed-gitlab-runner-prebuilt-tar-xz-no-such-file-or-directory
* Run the file:
```bash
sudo /usr/lib/gitlab-runner/mk-prebuilt-images.sh
```
* Run review:
```bash
gitlab-runner exec docker review
```

## Encore

While installing encore: https://symfony.com/doc/current/frontend/encore/installation.html

Running yarn was causing an issue:
```
$ yarn install
yarn install v1.13.0
info No lockfile found.
[1/4] Resolving packages...
[2/4] Fetching packages...
error webpack-manifest-plugin@2.0.4: The engine "node" is incompatible with this module. Expected version ">=6.11.5". Got "6.11.4"
error Found incompatible module
info Visit https://yarnpkg.com/en/docs/cli/install for documentation about this command.
```