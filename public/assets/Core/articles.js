var scrollMonitor = require('../vendor/scrollmonitor/index.js')

// scroll loaded feed in view on list of feeds
$('feeds').scrollTop = $("#feeds a[data-feed-id='{{ feedId }}']").offsetTop;

var containerElement = $("#content");

var containerMonitor = scrollMonitor.createContainer(containerElement);

var articles = $("#content article");

/**
 * Mark articles as read when user is scrolling trough the feed.
 * TODO: Articles seen only from the bottom should not count as read...
 */
const minTimeToReadArticle = 3000; // in ms
articles.each(function (i, article) {

    var timer;
    var enterTimestamp;

    var elementWatcher = containerMonitor.create(article);

    elementWatcher.enterViewport(function () {
        enterTimestamp = Date.now();
        // set timer to mark this article as read
        timer = setTimeout(function () {
            $.post(Routing.generate('deeprss_reader_core_stream_markasread', {'articleId': article.id}));
        }, minTimeToReadArticle);
    });

    elementWatcher.exitViewport(function () {
        // if the reader had not enough time to read article, it should not be marked as read
        if(Date.now() - enterTimestamp < minTimeToReadArticle) {
            clearTimeout(timer);
        }
    });

});

