#
# Makefile
#
# You can run it from command line by typing eg.:
#   make review
#
# You can read an user friendly introduction to Makefile basics here:
# https://gist.github.com/Isinlor/035399fe952f5e3ced4280a5cc635a84
#
# If you are intrested in exhaustive, but not so friendly manual:
# https://www.gnu.org/software/make/manual/make.html
#

# include .env file if one exists otherwise include .env.dist
# it makes sure that recipies depended on env variables will work
include $(shell ([ -f .env ] && echo ".env") || echo ".env.dist")

# allows to start php dev server
server: install
	php -S 127.0.0.1:8000 -t public

# provides an automatic review of the code
review: phplint validate-orm-mapping phpmd phpstan psalm phpcpd phpcs warmup

# checks syntax of source files and spec files
phplint:
	php -l ./src

# runs copy paste detection
phpcpd: install
	bin/phpcpd ./src --min-lines=1 --min-tokens=35

# runs PHP Code Sniffer
phpcs: install
	bin/phpcs ./src --standard=./vendor-bin/phpcs/phpcs.xml --runtime-set ignore_warnings_on_exit 1

# runs PHP Code Sniffer automatic fixer
phpcs-fix: install
	bin/phpcbf ./src --standard=./vendor-bin/phpcs/phpcs.xml --runtime-set ignore_warnings_on_exit 1

# runs PHP Mess Detector
phpmd: install
	bin/phpmd ./src text ./vendor-bin/phpmd/ruleset.xml

# runs PHPStan
phpstan: install
	bin/phpstan analyse ./src --level=max --no-progress

# runs Psalm
psalm: install
	bin/psalm --config=./vendor-bin/psalm/psalm.xml --show-info=false

# runs Phan
phan: install
	bin/phan --config-file ./vendor-bin/phan/config.php

# warms up cache and as a side effect checks big chunk of configuration
warmup: install
	bin/console cache:warmup

# validates doctrine mapping
validate-orm-mapping: install
	bin/console doctrine:schema:validate --skip-sync --quiet

# install dependencies allowing application to run
install: composer.lock vendor/autoload.php .env public/assets/vendor/.yarn-integrity .git/hooks/pre-commit

# update dependencies to match composer.json
composer.lock: composer.json
	composer update --lock --root-reqs --no-interaction --quiet

# install dependencies trough the composer (.env is created then too)
vendor/autoload.php .env:
	composer install --no-interaction --quiet

# install assets
public/assets/vendor/.yarn-integrity:
	yarn install --frozen-lockfile --silent --no-progress

# build docker container based on Dockerfile
# access with: docker run -t deeprss:php
docker: Dockerfile
	docker build ./ --tag deeprss:php

# check if git hooks exits; if it does then create pre commit hook by linking pre-commit.sh
.git/hooks/pre-commit:
	[ ! -d .git/hooks ] || [ -L .git/hooks/pre-commit ] || ln -s -f ../../pre-commit.sh .git/hooks/pre-commit

# disables predifined make rules and stops make from creating “install”
%: %.sh
    # more info: https://stackoverflow.com/a/15008209/893222
