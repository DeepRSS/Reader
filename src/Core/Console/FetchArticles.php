<?php

namespace DeepRSS\Reader\Core\Console;

use DeepRSS\Reader\Core\Domain\Collection\Feeds;
use DeepRSS\Reader\Core\Repository\FeedRepository;
use DeepRSS\Reader\Core\Service\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface as Input;
use Symfony\Component\Console\Output\OutputInterface as Output;

class FetchArticles extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var Reader
     */
    private $reader;

    /**
     * FetchArticles constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param FeedRepository $feedRepository
     * @param Reader $reader
     */
    public function __construct(EntityManagerInterface $entityManager, FeedRepository $feedRepository, Reader $reader)
    {
        $this->entityManager = $entityManager;
        $this->feedRepository = $feedRepository;
        $this->reader = $reader;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('reader:fetch-articles')
            ->setDescription('Fetch articles from feeds.');
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param Input $input
     * @param Output $output
     *
     * @return int|null|void
     */
    protected function execute(Input $input, Output $output)
    {

        $feeds = $this->feedRepository->getAll();

        $i = 1;
        $feedsToUpdate = [];
        foreach ($feeds as $feed) {
            if ($i % 5 === 0) { // update feeds in groups of 5
                $this->reader->updateAll(new Feeds($feedsToUpdate));
                $this->entityManager->flush();
                $feedsToUpdate = [];
            } else {
                $feedsToUpdate[] = $feed;
            }
            $i++;
        }

        // there may be still some remaining feeds to update
        $this->reader->updateAll(new Feeds($feedsToUpdate));
        $this->entityManager->flush();

        $output->writeln("Fetched articles from " . count($feeds->getIterator()) . " feeds.");

    }

}