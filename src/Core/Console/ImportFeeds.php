<?php

namespace DeepRSS\Reader\Core\Console;

use DeepRSS\Reader\Core\Service\FeedsImporter;
use DeepRSS\Reader\Core\Service\OpmlDecoder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface as Input;
use Symfony\Component\Console\Output\OutputInterface as Output;

class ImportFeeds extends Command
{

    /**
     * @var OpmlDecoder
     */
    private $opmlDecoder;

    /**
     * @var FeedsImporter
     */
    private $feedsImporter;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ImportFeeds constructor.
     *
     * @param OpmlDecoder $opmlDecoder
     * @param FeedsImporter $feedsImporter
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        OpmlDecoder $opmlDecoder,
        FeedsImporter $feedsImporter,
        EntityManagerInterface $entityManager
    ) {
        $this->opmlDecoder = $opmlDecoder;
        $this->feedsImporter = $feedsImporter;
        $this->entityManager = $entityManager;
        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->setName('reader:import-feeds')
            ->setDescription('Imports feeds from an opml file.')
            ->addArgument(
                'opml-file',
                InputArgument::REQUIRED,
                'Path to the file that needs to be imported.'
            );
    }

    protected function execute(Input $input, Output $output)
    {
        $opmlFile = $input->getArgument('opml-file');
        assert(is_string($opmlFile));
        $content = file_get_contents($opmlFile);

        if ($content === false) {
            throw new \InvalidArgumentException("Failed to read $opmlFile!");
        }

        $feeds = $this->feedsImporter->import(
            $this->opmlDecoder->getFeedHandles(
                $content
            )
        );

        $this->entityManager->flush();

        $output->writeln("Imported " . count($feeds->getIterator()) . " feeds.");
    }

}