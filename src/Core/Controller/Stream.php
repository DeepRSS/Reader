<?php

namespace DeepRSS\Reader\Core\Controller;

use DeepRSS\Reader\Core\Domain\Article;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Repository\ArticleRepository;
use DeepRSS\Reader\Core\Repository\FeedRepository;
use DeepRSS\Reader\Core\View\UserFeedView;
use DeepRSS\Reader\User\Domain\ArticleStatus;
use DeepRSS\Reader\User\Domain\User;
use DeepRSS\Reader\User\Repository\ArticleStatusRepository;
use DeepRSS\Reader\User\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Stream extends AbstractController
{

    /**
     * @Route("/feeds-list")
     *
     * @param UserRepository $userRepository
     * @param FeedRepository $feedRepository
     * @param ArticleStatusRepository $articleStatusRepository
     * @return Response
     */
    public function getFeeds(
        UserRepository $userRepository,
        FeedRepository $feedRepository,
        ArticleStatusRepository $articleStatusRepository
    ): Response {

        /** @var User $user */
        $user = $userRepository->getById('test');
        $feeds = $feedRepository->getAll();

        /** @var UserFeedView[] $userFeedViews */
        $userFeedViews = [];
        foreach ($feeds as $feed) {
            /** @var Feed $feed */
            $userFeedViews[] = new UserFeedView(
                $feed,
                $articleStatusRepository->countUnseenArticlesInFeed($feed, $user)
            );
        }

        return $this->render(
            "@Core/feeds.twig", [
                'userFeedViews' => $userFeedViews
            ]
        );
    }

    /**
     * @Route("/feed/{feedId}")
     * @Route("/feed")
     * @Route("/")
     *
     * @param FeedRepository $feedRepository
     * @param UserRepository $userRepository
     * @param ArticleRepository $articleRepository
     * @param string|null $feedId
     *
     * @return Response
     */
    public function getArticles(
        FeedRepository $feedRepository,
        UserRepository $userRepository,
        ArticleRepository $articleRepository,
        string $feedId = null
    ): Response {

        if ($feedId === null) {
            $feedId = $feedRepository->getAll()->getFirst()->getId();
        }

        /** @var Feed $feed */
        $feed = $feedRepository->getById($feedId);

        /** @var User $user */
        $user = $userRepository->getById('test');

        $seenArticles = $articleRepository->getSeenInFeed($feed, $user);
        $unseenArticles = $articleRepository->getUnseenInFeed($feed, $user);

        return $this->render(
            "@Core/articles.twig", [
                'seenArticles' => $seenArticles,
                'unseenArticles' => $unseenArticles,
                'feedId' => $feedId
            ]
        );

    }

    /**
     * @Route("/article/{articleId}/markAsRead", methods={"POST"}, options={"expose"=true})
     *
     * @param ArticleRepository $articleRepository
     * @param UserRepository $userRepository
     * @param string $articleId
     *
     * @return Response
     */
    public function markAsRead(
        ArticleRepository $articleRepository,
        UserRepository $userRepository,
        string $articleId
    ): Response {

        /** @var User $user */
        $user = $userRepository->getById('test');

        /** @var Article $article */
        $article = $articleRepository->getById($articleId);

        $articleStatus = new ArticleStatus($user, $article);

        $this->getDoctrine()->getManager()->persist($articleStatus);
        $this->getDoctrine()->getManager()->flush();

        return new Response();

    }

}
