<?php
declare(strict_types=1);

namespace DeepRSS\Reader\Core\View;

use DeepRSS\Reader\Core\Domain\Feed;

class UserFeedView
{

    /**
     * @var Feed
     */
    public $feed;

    /**
     * @var int
     */
    public $unseenArticlesCount;

    /**
     * UserFeedView constructor.
     *
     * @param Feed $feed
     * @param int $unseenArticlesCount
     */
    public function __construct(Feed $feed, int $unseenArticlesCount)
    {
        $this->feed = $feed;
        $this->unseenArticlesCount = $unseenArticlesCount;
    }

}
