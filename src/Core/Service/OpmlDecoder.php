<?php

namespace DeepRSS\Reader\Core\Service;

use DeepRSS\Reader\Core\Domain\Collection\FeedHandles;
use DeepRSS\Reader\Core\Domain\FeedHandle;
use DeepRSS\Reader\Core\Domain\Url;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Allows to extract list of feed handles from OPML file content.
 *
 * TODO: Provide contract?
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class OpmlDecoder
{

    /**
     * @var DecoderInterface
     */
    private $decoder;

    /**
     * OpmlDecoder constructor.
     *
     * @param DecoderInterface $decoder
     */
    public function __construct(DecoderInterface $decoder)
    {
        // TODO assert $this->decoder->supportsDecoding('xml');
        $this->decoder = $decoder;
    }

    /**
     * @param string $opml
     *
     * @return FeedHandles
     */
    public function getFeedHandles(string $opml): FeedHandles
    {
        return new FeedHandles(
            $this->getFeedHandlesArray(
                $this->decoder->decode($opml, 'xml')['body']
            )
        );
    }

    /**
     * @param array $outline
     *
     * @return array
     */
    private function getFeedHandlesArray(array $outline): array
    {

        $feedHandles = [];

        if (isset($outline['outline'])) {
            $feedHandles = $this->getFeedHandlesArray($outline['outline']);
        }

        foreach ($outline as $item) {

            if (isset($item['outline'])) {
                $feedHandles = array_merge($feedHandles, $this->getFeedHandlesArray($item['outline']));
            }

            if (isset($item['@text'], $item['@xmlUrl'])) {

                // TODO: Consider FeedHandle label "$item['@text']"
                $feedHandles[] = new FeedHandle(
                    Url::createSanitized($item['@xmlUrl'])
                );

            }

        }

        return $feedHandles;

    }

}