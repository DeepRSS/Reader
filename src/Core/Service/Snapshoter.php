<?php

namespace DeepRSS\Reader\Core\Service;

use DeepRSS\Reader\Core\Contract;
use DeepRSS\Reader\Core\Contract\Exception\ParseError;
use DeepRSS\Reader\Core\Contract\Snapshot;
use DeepRSS\Reader\Core\Domain\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\EachPromise;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Allows to take snapshoots based on feed url.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Snapshoter implements Contract\Snapshoter, Contract\MassSnapshoter
{

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var Contract\FeedParser
     */
    private $parser;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Snapshoter constructor.
     *
     * @param ClientInterface $client
     * @param Contract\FeedParser $parser
     * @param LoggerInterface $logger
     */
    public function __construct(ClientInterface $client, Contract\FeedParser $parser, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->parser = $parser;
        $this->logger = $logger;
    }

    /**
     * TODO: Provide documentation.
     *
     * @param string $feedUrl
     *
     * @return Snapshot
     *
     * @throws ParseError
     */
    public function takeSnapshot(string $feedUrl): Snapshot
    {
        $response = $this->client->request('GET', $feedUrl);

        return $this->parser->parse($feedUrl, (string)$response->getBody());
    }

    /**
     * TODO: Provide documentation.
     *
     * @param string[]|Url[] $feedUrls
     * @param int $concurrency
     *
     * @return array
     */
    public function takeSnapshots(array $feedUrls, int $concurrency = 4): array
    {

        $this->logger->info(
            "Start taking snapshots based on " . count($feedUrls) .
            " feed urls with $concurrency concurrency."
        );

        $snapshots = [];

        $promise = (new EachPromise(
            $this->getPromisesIterator($feedUrls),
            [
                'concurrency' => $concurrency,
                'fulfilled' => function (array $result) use (&$snapshots) {

                    /** @var string $feedUrl */
                    $feedUrl = $result['feedUrl'];

                    if (isset($result['exception'])) {
                        $this->logger->error("Network error occurred when taking snapshot of $feedUrl.", $result);
                        return;
                    }

                    /** @var ResponseInterface $response */
                    $response = $result['response'];

                    try {

                        $snapshots[] = $this->parser->parse($feedUrl, (string)$response->getBody());
                        $this->logger->debug("Snapshot of $feedUrl taken successfully.");

                    } catch (ParseError $e) {
                        $result['exception'] = $e;
                        $this->logger->error("Parse error occurred when taking snapshot of $feedUrl.", $result);
                    }

                },
            ]
        ))->promise();

        $promise->wait();

        $this->logger->info(
            "Successfully finished taking snapshots of " . count($snapshots) . " feeds. " .
            count($feedUrls) . " feeds requested. " . (count($feedUrls) - count($snapshots)) . " feeds failed."
        );

        return $snapshots;
    }

    /**
     * @param string[]|Url[] $feedUrls
     *
     * @return \Iterator
     */
    private function getPromisesIterator(array $feedUrls): \Iterator
    {
        foreach ($feedUrls as $feedUrl) {
            // $feedUrl casting is necessary since Guzzle may reject string like objects
            yield $this->client->requestAsync('GET', (string)$feedUrl)
                ->then(
                    function (ResponseInterface $response) use ($feedUrl) {
                        return ['response' => $response, 'feedUrl' => $feedUrl];
                    },
                    function (\Throwable $e) use ($feedUrl) {
                        return ['exception' => $e, 'feedUrl' => $feedUrl];
                    }
                );
        }
    }

}