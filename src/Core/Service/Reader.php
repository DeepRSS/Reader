<?php

namespace DeepRSS\Reader\Core\Service;

use DeepRSS\Reader\Core\Contract\Entry;
use DeepRSS\Reader\Core\Contract\MassSnapshoter;
use DeepRSS\Reader\Core\Contract\Snapshot;
use DeepRSS\Reader\Core\Domain\Collection\Feeds;
use DeepRSS\Reader\Core\Domain\Collection\Articles;
use DeepRSS\Reader\Core\Domain\Article;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Domain\Url;
use DeepRSS\Reader\Core\Repository\ArticleRepository;
use DeepRSS\Reader\Utilities\Contract\ContentExtractor;
use DeepRSS\Reader\Utilities\Contract\Purifier;
use Psr\Log\LoggerInterface;

/**
 * Allows to update articles of specified feeds.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Reader
{

    /**
     * @var MassSnapshoter
     */
    private $snapshoter;

    /**
     * @var Purifier
     */
    private $purifier;

    /**
     * @var ContentExtractor
     */
    private $extractor;

    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Reader constructor.
     *
     * @param MassSnapshoter $snapshoter
     * @param Purifier $purifier
     * @param ContentExtractor $extractor
     * @param ArticleRepository $articleRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        MassSnapshoter $snapshoter,
        Purifier $purifier,
        ContentExtractor $extractor,
        ArticleRepository $articleRepository,
        LoggerInterface $logger
    ) {
        $this->snapshoter = $snapshoter;
        $this->purifier = $purifier;
        $this->extractor = $extractor;
        $this->articleRepository = $articleRepository;
        $this->logger = $logger;
    }

    /**
     * TODO: Provide documentation.
     *
     * @param Feeds $feeds
     *
     * @return void
     */
    public function updateAll(Feeds $feeds): void
    {
        $snapshots = $this->snapshoter->takeSnapshots($feeds->getUrls());

        foreach ($snapshots as $snapshot) {

            $this->saveArticlesFromSnapshot(
                $feeds->getOneByFeedUrl(
                    Url::createSanitized($snapshot->getUrl())
                ),
                $snapshot
            );

        }

    }

    /**
     * TODO: Provide documentation.
     *
     * @param Feed $feed
     *
     * @return void
     */
    public function update(Feed $feed): void
    {

        $snapshot = $this->snapshoter->takeSnapshot((string)$feed->getUrl());

        $this->saveArticlesFromSnapshot($feed, $snapshot);

    }

    /**
     * @param Feed $feed
     * @param Snapshot $snapshot
     */
    private function saveArticlesFromSnapshot(Feed $feed, Snapshot $snapshot): void
    {
        $articles = $this->processSnapshot($feed, $snapshot);

        $this->articleRepository->saveAll($articles);
    }

    /**
     * TODO: Provide documentation.
     *
     * @param Feed $feed
     * @param Snapshot $snapshot
     *
     * @return Articles
     */
    private function processSnapshot(Feed $feed, Snapshot $snapshot): Articles
    {

        $articles = [];
        foreach ($snapshot->getEntries() as $entry) {

            try {

                $articles[] = $this->processEntry($feed, $entry);

            } catch (\Throwable $e) {

                // TODO: Implement proper error handling
                $this->logger->error("An issue while reading article: {$e->getMessage()}", ['exception' => $e]);

            }

        }

        return new Articles($articles);

    }

    /**
     * TODO: Provide documentation.
     *
     * @param Feed $feed
     * @param Entry $entry
     *
     * @return Article
     */
    private function processEntry(Feed $feed, Entry $entry): Article
    {

        $articleUrl = Url::createSanitized($entry->getUrl());

        // TODO Take feed into account
        $article = $this->articleRepository->findByUrl($articleUrl);

        $providedContent = $this->purifier->purify($entry->getContent());

//        $extractedContent = $this->extractor->extract((string)$entry->getLink());
//        $extractedContent = $this->purifier->purify($extractedContent);

//        $content = strlen($providedContent) > strlen($extractedContent) ? $providedContent : $extractedContent;

        $content = $providedContent;

        /** @var mixed[] $data */
        $data = [
            $this->purifier->purify($entry->getTitle()),
            $content,
            $entry->getPublishedAt(),
        ];

        if ($article instanceof Article) {
            $article->refresh(...$data);
            return $article;
        }

        array_unshift($data, $feed);
        $data[] = $articleUrl;

        return new Article(...$data);

    }

}