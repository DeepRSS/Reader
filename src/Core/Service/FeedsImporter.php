<?php

namespace DeepRSS\Reader\Core\Service;

use DeepRSS\Reader\Core\Domain\Collection\FeedHandles;
use DeepRSS\Reader\Core\Domain\Collection\Feeds;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Repository\FeedRepository;

/**
 * Allows to import feeds in bulk.
 *
 * TODO: Add implementation independent contract?
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class FeedsImporter
{

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * FeedsImporter constructor.
     *
     * @param FeedRepository $feedRepository
     */
    public function __construct(FeedRepository $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }

    /**
     * Imports feeds to long term storage based on feed handles.
     *
     * Makes sure that for each url a feed is imported only once.
     *
     * @param FeedHandles $handles
     *
     * @return Feeds
     */
    public function import(FeedHandles $handles): Feeds
    {

        $feeds = [];
        foreach ($handles->getWithUniqueUrls() as $handle) {

            $feed = $this->feedRepository->findByUrl($handle->getUrl());

            if (!$feed instanceof Feed) {

                $feed = new Feed($handle->getUrl());
                $this->feedRepository->save($feed);

            }

            $feeds[] = $feed;

        }

        return new Feeds($feeds);

    }

}