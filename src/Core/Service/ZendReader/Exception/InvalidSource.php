<?php

namespace DeepRSS\Reader\Core\Service\ZendReader\Exception;

use DeepRSS\Reader\Core\Contract\Exception\ParseError;

/**
 * Invalid source can not be parsed.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class InvalidSource extends \RuntimeException implements ParseError
{
}
