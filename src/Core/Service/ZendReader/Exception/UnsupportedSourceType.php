<?php

namespace DeepRSS\Reader\Core\Service\ZendReader\Exception;

/**
 * Unsupported source type can not be parsed.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class UnsupportedSourceType extends InvalidSource
{
}
