<?php

namespace DeepRSS\Reader\Core\Service\ZendReader;

use DeepRSS\Reader\Core\Contract\Entry;
use Zend\Feed\Reader\Entry\EntryInterface;

/**
 * Proxy object for Zend Entry.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class EntryWrapper implements Entry
{

    /**
     * @var EntryInterface
     */
    private $entry;

    /**
     * EntryWrapper constructor.
     *
     * @param EntryInterface $entry
     */
    public function __construct(EntryInterface $entry)
    {
        $this->entry = $entry;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->entry->getTitle();
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->entry->getLink();
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->entry->getContent();
    }

    /**
     * @return int
     */
    public function getPublishedAt(): int
    {
        // TODO: Handle $this->entry->getDateCreated() being null
        return $this->entry->getDateCreated()->getTimestamp();
    }

}