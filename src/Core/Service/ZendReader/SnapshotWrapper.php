<?php

namespace DeepRSS\Reader\Core\Service\ZendReader;

use DeepRSS\Reader\Core\Contract\Snapshot;
use Zend\Feed\Reader\Entry\EntryInterface;
use Zend\Feed\Reader\Feed\FeedInterface;

class SnapshotWrapper implements Snapshot
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var FeedInterface
     */
    private $feed;

    /**
     * SnapshotWrapper constructor.
     *
     * @param string        $url
     * @param FeedInterface $feed
     */
    public function __construct($url, FeedInterface $feed)
    {
        $this->url = $url;
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function getEntries(): array
    {
        $entries = [];
        
        /** @var EntryInterface $entry */
        foreach ($this->feed as $entry) {
            $entries[] = new EntryWrapper($entry);
        }

        return $entries;
    }

}