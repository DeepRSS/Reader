<?php

namespace DeepRSS\Reader\Core\Service\ZendReader;

use DeepRSS\Reader\Core\Contract\Snapshot;
use Zend\Feed\Reader\Entry\EntryInterface;

class EntryToSnapshotWrapper implements Snapshot
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var EntryInterface
     */
    private $entry;

    /**
     * EntryToSnapshotWrapper constructor.
     *
     * @param string         $url
     * @param EntryInterface $entry
     */
    public function __construct($url, EntryInterface $entry)
    {
        $this->url = $url;
        $this->entry = $entry;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function getEntries(): array
    {
        return [
            new EntryWrapper($this->entry)
        ];
    }

}