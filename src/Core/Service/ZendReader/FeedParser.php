<?php

namespace DeepRSS\Reader\Core\Service\ZendReader;

use DeepRSS\Reader\Core\Contract;
use DeepRSS\Reader\Core\Contract\Exception\ParseError;
use DeepRSS\Reader\Core\Contract\Snapshot;
use DeepRSS\Reader\Core\Service\ZendReader\Exception\InvalidSource;
use DeepRSS\Reader\Core\Service\ZendReader\Exception\UnsupportedSourceType;
use Zend\Feed\Exception\ExceptionInterface;
use Zend\Feed\Reader\Entry;
use Zend\Feed\Reader\Feed;

/**
 * An extension to Zend Feed Reader implementing FeedParser contract.
 *
 * Main extensions:
 *  - allows to parse slightly malformatted feeds i.e. $dom->recover = true
 *  - output with uniform interface in form of Snapshot
 *  - wraps zend exceptions
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class FeedParser extends \Zend\Feed\Reader\Reader implements Contract\FeedParser
{

    /**
     * @param string $feedUrl
     * @param string $data
     *
     * @return Snapshot
     *
     * @throws ParseError
     */
    public function parse(string $feedUrl, string $data): Snapshot
    {
        /** @psalm-suppress InvalidCatch */
        try {

            $dom = $this->getDomDocumentFromString($data);

            $type = static::detectType($dom);

            static::registerCoreExtensions();

            if (substr($type, 0, 3) === 'rss') {

                $feed = new SnapshotWrapper(
                    $feedUrl, new Feed\Rss($dom, $type)
                );

            } elseif (substr($type, 8, 5) === 'entry') {

                $feed = new EntryToSnapshotWrapper(
                    $feedUrl, new Entry\Atom($dom->documentElement, 0, self::TYPE_ATOM_10)
                );

            } elseif (substr($type, 0, 4) === 'atom') {

                $feed = new SnapshotWrapper(
                    $feedUrl, new Feed\Atom($dom, $type)
                );

            } else {
                throw new UnsupportedSourceType(
                    "Given source is not a valid Atom, RSS or RDF feed that Zend\Feed\Reader can parse."
                );
            }

        } catch (ExceptionInterface $e) {
            /** @phan-suppress-next-line PhanTypeMismatchArgumentInternal */
            throw new InvalidSource("Zend\Feed\Reader can't parse given source.", 0, $e);
        }

        return $feed;
    }

    /**
     * TODO: Provide documentation.
     *
     * @param string $feedString
     *
     * @return \DOMDocument
     *
     * @throws InvalidSource
     */
    protected function getDomDocumentFromString(string $feedString): \DOMDocument
    {

        $feedString = trim($feedString); // TODO Unicode trim

        if (empty($feedString)) {
            throw new InvalidSource("Empty source given.");
        }

        $libxmlErrflag = libxml_use_internal_errors(true);
        $oldValue = libxml_disable_entity_loader(true);
        $dom = new \DOMDocument;
        $dom->recover = true; // Allows to parse slightly malformatted feeds

        $status = $dom->loadXML($feedString);

        foreach ($dom->childNodes as $child) {
            // Added as part of XXE attack prevention patch. More info:
            // https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet#PHP
            // https://github.com/zendframework/zend-feed/commit/2c36a1340d764ab8219b5e1276298faee134fd27
            // TODO Is it still relevant? What about libxml_disable_entity_loader(true);?
            if ($child->nodeType === XML_DOCUMENT_TYPE_NODE) {
                throw new InvalidSource(
                    "Invalid XML: Detected use of illegal DOCTYPE. This may be potential XEE attack."
                );
            }
        }

        libxml_disable_entity_loader($oldValue);
        libxml_use_internal_errors($libxmlErrflag);

        if (!$status) {

            // Build error message
            $error = libxml_get_last_error();
            if ($error instanceof \LibXMLError && $error->message != '') {
                $error->message = trim($error->message);
                $errormsg = "DOMDocument cannot parse XML: {$error->message}";
            } else {
                $errormsg = "DOMDocument cannot parse XML: Please check the XML document's validity";
            }

            throw new InvalidSource($errormsg);
        }

        return $dom;

    }

}