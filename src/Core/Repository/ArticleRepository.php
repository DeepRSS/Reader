<?php

namespace DeepRSS\Reader\Core\Repository;

use DeepRSS\Reader\Core\Domain\Article;
use DeepRSS\Reader\Core\Domain\Collection\Articles;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Domain\Url;
use DeepRSS\Reader\User\Domain\User;
use DeepRSS\Reader\Utilities\AbstractDoctrineRepository;

/**
 * Provides access to long term storage of articles.
 *
 * @see Article
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class ArticleRepository extends AbstractDoctrineRepository
{

    /**
     * @param Articles $articles
     *
     * @return void
     */
    public function saveAll(Articles $articles): void
    {
        foreach ($articles as $article) {
            $this->save($article);
        }
    }

    /**
     * @param Url $url
     *
     * @return Article|null
     */
    public function findByUrl(Url $url): ?Article
    {
        return $this->gateway->findOneBy([
            'url.url' => (string)$url // TODO Doctrine goes high wire with VO
        ]);
    }

    /**
     * @param Feed $feed
     * @param User $user
     *
     * @return Articles
     */
    public function getSeenInFeed(Feed $feed, User $user): Articles
    {
        $articles = $this->gateway
            ->getEntityManager()
            ->createQuery(/** @lang DQL */
                "SELECT ca FROM Core:Article AS ca WHERE ca.feed = :feed AND ca.id IN (
                     SELECT IDENTITY(uas.article) FROM User:ArticleStatus AS uas WHERE uas.user = :user AND uas.seen = 1
                )"
            )
            ->setParameters([
                'feed' => $feed,
                'user' => $user,
            ])
            ->getResult();

        return new Articles($articles);
    }

    /**
     * @param Feed $feed
     * @param User $user
     *
     * @return Articles
     */
    public function getUnseenInFeed(Feed $feed, User $user): Articles
    {
        $articles = $this->gateway
            ->getEntityManager()
            ->createQuery(/** @lang DQL */
                "SELECT ca FROM Core:Article AS ca WHERE ca.feed = :feed AND ca.id NOT IN (
                     SELECT IDENTITY(uas.article) FROM User:ArticleStatus AS uas WHERE uas.user = :user AND uas.seen = 1
                )"
            )
            ->setParameters([
                'feed' => $feed,
                'user' => $user,
            ])
            ->getResult();

        return new Articles($articles);
    }

    protected function getEntityClass(): string
    {
        return Article::class;
    }

}
