<?php

namespace DeepRSS\Reader\Core\Repository;

use DeepRSS\Reader\Core\Domain\Collection\Feeds;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Domain\Url;
use DeepRSS\Reader\Utilities\AbstractDoctrineRepository;

/**
 * Provides access to long term storage of feeds.
 *
 * @see Feed
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class FeedRepository extends AbstractDoctrineRepository
{

    /**
     * @return Feeds
     */
    public function getAll(): Feeds
    {
        return new Feeds($this->gateway->findAll());
    }

    /**
     * @param Url $url
     *
     * @return Feed|null
     */
    public function findByUrl(Url $url): ?Feed
    {
        return $this->gateway->findOneBy([
            'url.url' => (string)$url // TODO Doctrine goes high wire with VO
        ]);
    }

    protected function getEntityClass(): string
    {
        return Feed::class;
    }

}