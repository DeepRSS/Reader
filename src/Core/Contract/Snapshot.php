<?php

namespace DeepRSS\Reader\Core\Contract;

/**
 * Allows to provides uniform representation of state of a feed in a given moment.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Snapshot
{

    /**
     * Must return the original url used to obtain the snapshot.
     *
     * @return string
     */
    public function getUrl();

    /**
     * @return Entry[]
     */
    public function getEntries(): array;

}