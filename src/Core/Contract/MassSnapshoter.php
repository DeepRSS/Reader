<?php

namespace DeepRSS\Reader\Core\Contract;

/**
 * Allows to take snapshot of many feeds at once.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface MassSnapshoter extends Snapshoter
{

    /**
     * @param string[] $feedUrls
     *
     * @return Snapshot[]
     */
    public function takeSnapshots(array $feedUrls): array;

}