<?php

namespace DeepRSS\Reader\Core\Contract;

use DeepRSS\Reader\Core\Contract\Exception\ParseError;

/**
 * Allows to produce snapshot of a feed based on raw text representation of a feed.
 *
 * TODO: Should original feed url be required?
 * TODO: Implementations of feed parses won't do anything else with an URL than just pass it to the snapshot.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface FeedParser
{

    /**
     * @param string $originalFeedUrl
     * @param string $source A raw, text based representation of a feed.
     *
     * @return Snapshot
     *
     * @throws ParseError in case a Snapshot can not be created.
     */
    public function parse(string $originalFeedUrl, string $source): Snapshot;

}