<?php

namespace DeepRSS\Reader\Core\Contract\Exception;

use DeepRSS\Reader\Utilities\Contract\Exception\Common;

/**
 * Represents an issue with parsing source of a feed.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface ParseError extends Common
{
}
