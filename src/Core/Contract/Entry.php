<?php

namespace DeepRSS\Reader\Core\Contract;

/**
 * Common interface for unprocessed entry from a feed.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Entry
{

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * Returns timestamp of reported creation / publication date.
     *
     * @return int
     */
    public function getPublishedAt(): int;

}