<?php

namespace DeepRSS\Reader\Core\Contract;

/**
 * Creates an uniform representation of state of a feed in a given moment.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Snapshoter
{

    /**
     * @param string $feedUrl
     *
     * @return Snapshot
     */
    public function takeSnapshot(string $feedUrl): Snapshot;

}