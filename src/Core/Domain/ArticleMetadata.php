<?php

namespace DeepRSS\Reader\Core\Domain;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class ArticleMetadata
{

    /**
     * @ORM\Column
     *
     * @var string
     */
    private $hash;

    /**
     * Claimed date of publication.
     *
     * @ORM\Column(type="integer")
     *
     * @var int timestamp
     */
    private $publishedAt;

    /**
     * Recorded date of retrieval of the last (current) version.
     *
     * @ORM\Column(type="integer")
     *
     * @var int timestamp
     */
    private $retrievedAt;

    /**
     * Number of recorder versions of the article.
     *
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $version;

    /**
     * ArticleMetadata constructor.
     *
     * @param string               $title
     * @param string               $content
     * @param int                  $publishedAt
     *
     * @param ArticleMetadata|null $oldMetadata
     */
    public function __construct(string $title, string $content, int $publishedAt, ArticleMetadata $oldMetadata = null)
    {

        $this->retrievedAt = time();

        // an article can not be retrieved before it is published
        // in case of any issues a decent default is the retrieval time
        $this->publishedAt = $publishedAt <= $this->retrievedAt ? $publishedAt : $this->retrievedAt;

        $this->hash = static::computeHash(
            [
            'title' => $title,
            'content' => $content
            ]
        );

        if ($oldMetadata instanceof self) {
            $this->version = $oldMetadata->getVersion();
            if ($this->hash !== $oldMetadata->getHash()) {
                $this->version++;
            }
        } else {
            $this->version = 1;
        }

    }

    /**
     * @return int
     */
    public function getRetrievedAt(): int
    {
        return $this->retrievedAt;
    }

    /**
     * @return int
     */
    public function getPublishedAt(): int
    {
        return $this->publishedAt;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * TODO Provide documentation.
     * TODO move to Hash value object
     *
     * @param array $data
     *
     * @return string
     */
    private static function computeHash(array $data): string
    {
        /**
 * @var string $serialized
*/
        $serialized = json_encode($data);
        Assert::stringNotEmpty($serialized);
        return hash("sha256", $serialized);
    }

}