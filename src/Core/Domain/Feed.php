<?php

namespace DeepRSS\Reader\Core\Domain;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Source of articles that should be periodically processed.
 *
 * @ORM\Entity
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Feed
{

    /**
     * @ORM\Id
     * @ORM\Column
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\Embedded(class="Url")
     *
     * @var Url
     */
    private $url;

    /**
     * Feed constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

}