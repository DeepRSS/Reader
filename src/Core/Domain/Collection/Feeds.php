<?php

namespace DeepRSS\Reader\Core\Domain\Collection;

use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\Core\Domain\Url;
use DeepRSS\Reader\Utilities\Exception\EntityNotFound;
use RuntimeException;

/**
 * TODO: Add documentation.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Feeds implements \IteratorAggregate
{

    /**
     * @var Feed[]
     */
    private $feeds;

    /**
     * Feeds constructor.
     *
     * @param Feed[] $feeds
     */
    public function __construct(array $feeds)
    {
        $this->feeds = $feeds;
    }

    /**
     * @param Url $url
     *
     * @return Feed
     */
    public function getOneByFeedUrl(Url $url): Feed
    {
        // TODO handle duplicates
        foreach ($this as $feed) {
            if ((string)$feed->getUrl() === (string)$url) {
                return $feed;
            }
        }

        throw EntityNotFound::searchFailed("feed", "url: {$url}");
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        $urls = [];
        foreach ($this as $feed) {
            $urls[] = $feed->getUrl();
        }
        return $urls;
    }

    /**
     * @return Feed
     */
    public function getOne(): Feed
    {
        if (count($this->feeds) !== 1) {
            throw new RuntimeException(
                "Attempted to retrieve one and only one feed, but there are: " . count($this->feeds) . " feeds!"
            );
        }

        return $this->getFirst();
    }

    /**
     * @return Feed
     */
    public function getFirst(): Feed
    {
        return current($this->feeds);
    }

    /**
     * Allows to traverse feeds in foreach loop.
     *
     * @see http://php.net/manual/en/class.iteratoraggregate.php
     *
     * @return Feed[]|\ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->feeds);
    }

}
