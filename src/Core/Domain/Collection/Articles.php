<?php

namespace DeepRSS\Reader\Core\Domain\Collection;

use DeepRSS\Reader\Core\Domain\Article;
use DeepRSS\Reader\Core\Domain\FeedHandle;

/**
 * TODO: Add documentation.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Articles implements \IteratorAggregate
{

    /**
     * @var Article[]
     */
    private $articles;

    /**
     * Articles constructor.
     *
     * @param Article[] $articles
     */
    public function __construct(array $articles)
    {
        $this->articles = $articles;
    }

    /**
     * Allows to traverse articles in foreach loop.
     *
     * @see http://php.net/manual/en/class.iteratoraggregate.php
     *
     * @return FeedHandle[]|\ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->articles);
    }

}