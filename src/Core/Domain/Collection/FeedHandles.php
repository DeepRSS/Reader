<?php

namespace DeepRSS\Reader\Core\Domain\Collection;

use DeepRSS\Reader\Core\Domain\FeedHandle;

/**
 * TODO: Add documentation.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>s
 */
class FeedHandles implements \IteratorAggregate
{

    /**
     * @var FeedHandle[]
     */
    private $feedHandles;

    /**
     * FeedHandles constructor.
     *
     * @param FeedHandle[] $handles
     */
    public function __construct(array $handles)
    {
        $this->feedHandles = $handles;
    }

    /**
     * Returns feed handles with unique urls.
     *
     * @return FeedHandle[]|static
     */
    public function getWithUniqueUrls()
    {
        $uniqueHandles = [];
        foreach ($this as $handle) {
            $uniqueHandles[(string)$handle->getUrl()] = $handle;
        }
        return new static(array_values($uniqueHandles));
    }

    /**
     * Allows to traverse feeds in foreach loop.
     *
     * @see http://php.net/manual/en/class.iteratoraggregate.php
     *
     * @return FeedHandle[]|\ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->feedHandles);
    }

}