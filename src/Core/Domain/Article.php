<?php

namespace DeepRSS\Reader\Core\Domain;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

/**
 * Represents the most recent version of content fetched from a feed.
 *
 * The relation between article versions is maintained trough the article url.
 *
 * TODO: Make sure that URL stays unique? Or URL and Feed?
 * TODO: Consider situation where slightly different article can be obtained from two feeds, but with the same url.
 *
 * @ORM\Entity
 *
 * @see ArticleRepository
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Article
{

    /**
     * @ORM\Id
     * @ORM\Column
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Feed")
     *
     * @var Feed
     */
    private $feed;

    /**
     * @ORM\Column
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $content;

    /**
     * @ORM\Embedded(class="Url")
     *
     * @var Url
     */
    private $url;

    /**
     * @ORM\Embedded(class="ArticleMetadata")
     *
     * @var ArticleMetadata
     */
    private $metadata;

    /**
     * Article constructor.
     *
     * @param Feed   $feed
     * @param string $title
     * @param string $content
     * @param int    $publishedAt
     * @param Url    $url
     */
    public function __construct(Feed $feed, string $title, string $content, int $publishedAt, Url $url)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->url = $url;
        $this->feed = $feed;
        $this->refresh($title, $content, $publishedAt);
    }

    /**
     * Refresh article to the most recent version obtained from the feed.
     *
     * @param string $title
     * @param string $content
     * @param int    $publishedAt
     *
     * @return void
     */
    public function refresh(string $title, string $content, int $publishedAt): void
    {
        Assert::notEmpty($title, "Title of the article: {$this->url} must not be empty!");

        $this->title = $title;
        $this->content = $content;

        $this->metadata = new ArticleMetadata($title, $content, $publishedAt, $this->metadata);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @return ArticleMetadata
     */
    public function getMetadata(): ArticleMetadata
    {
        return $this->metadata;
    }

}