<?php

namespace DeepRSS\Reader\Core\Domain;

use Doctrine\ORM\Mapping as ORM;
use League\Uri\Http as LeagueUrl;

/**
 * TODO: Provide documentation.
 *
 * @ORM\Embeddable
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class Url
{

    /**
     * @ORM\Column
     *
     * @var LeagueUrl
     */
    private $url;

    /**
     * Url constructor.
     *
     * @param LeagueUrl $url
     */
    private function __construct(LeagueUrl $url)
    {
        $this->url = $url;
    }

    public static function createSanitized(string $url): self
    {
        return new static(LeagueUrl::createFromString($url));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->url;
    }

}