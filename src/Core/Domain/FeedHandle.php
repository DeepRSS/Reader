<?php

namespace DeepRSS\Reader\Core\Domain;

/**
 * TODO: In the current format this seems obsolete. Originally representing feed imported from OPML file.
 * TODO: However, it is certain that users will want to name their feeds.
 * TODO: Feeds are also named in OPML files.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class FeedHandle
{

    /**
     * @var Url
     */
    private $url;

    /**
     * FeedHandle constructor.
     *
     * @param Url    $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

}