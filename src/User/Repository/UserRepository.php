<?php

namespace DeepRSS\Reader\User\Repository;

use DeepRSS\Reader\User\Domain\User;
use DeepRSS\Reader\Utilities\AbstractDoctrineRepository;

/**
 * Provides access to long term storage of users.
 *
 * @see User
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class UserRepository extends AbstractDoctrineRepository
{

    protected function getEntityClass(): string
    {
        return User::class;
    }

}