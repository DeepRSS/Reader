<?php

namespace DeepRSS\Reader\User\Repository;

use DeepRSS\Reader\Core\Domain\Article;
use DeepRSS\Reader\Core\Domain\Collection\Articles;
use DeepRSS\Reader\Core\Domain\Feed;
use DeepRSS\Reader\User\Domain\ArticleStatus;
use DeepRSS\Reader\User\Domain\User;
use DeepRSS\Reader\Utilities\AbstractDoctrineRepository;

/**
 * Provides access to long term storage of articles statuses.
 *
 * @see ArticleStatus
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class ArticleStatusRepository extends AbstractDoctrineRepository
{

    /**
     * @param Feed $feed
     * @param User $user
     *
     * @return int
     */
    public function countUnseenArticlesInFeed(Feed $feed, User $user): int
    {
        $totalArticles = $this->gateway->getRepository(Article::class)
            ->count([
                'feed' => $feed,
            ]);

        $seenArticles = $this->gateway
            ->getEntityManager()
            ->createQuery(/** @lang DQL */
                "SELECT COUNT(ca) FROM Core:Article AS ca WHERE ca.feed = :feed AND ca.id IN (
                     SELECT IDENTITY(uas.article) FROM User:ArticleStatus AS uas WHERE uas.user = :user AND uas.seen = 1
                )"
            )
            ->setParameters([
                'feed' => $feed,
                'user' => $user,
            ])
            ->getSingleScalarResult();

        return $totalArticles - $seenArticles;
    }

    protected function getEntityClass(): string
    {
        return ArticleStatus::class;
    }

}
