<?php

namespace DeepRSS\Reader\User\Domain;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Represents the person using the reader.
 *
 * @ORM\Entity
 *
 * @see UserRepository
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\Column
     *
     * @var string
     */
    private $id;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

}