<?php

namespace DeepRSS\Reader\User\Domain;

use DeepRSS\Reader\Core\Domain\Feed;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Represents status of an article for an user.
 *
 * @ORM\Entity
 *
 * @see UserRepository
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class FeedStatus
{

    /**
     * @ORM\Id
     * @ORM\Column
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User")
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="DeepRSS\Reader\Core\Domain\Feed")
     *
     * @var Feed
     */
    private $feed;

    /**
     * FeedStatus constructor.
     *
     * @param User $user
     * @param Feed $feed
     */
    public function __construct(User $user, Feed $feed)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->user = $user;
        $this->feed = $feed;
    }

}