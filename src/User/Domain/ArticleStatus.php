<?php

namespace DeepRSS\Reader\User\Domain;

use DeepRSS\Reader\Core\Domain\Article;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Represents status of an article for an user.
 *
 * @ORM\Entity
 *
 * @see UserRepository
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class ArticleStatus
{

    /**
     * @ORM\Id
     * @ORM\Column
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="DeepRSS\Reader\Core\Domain\Article")
     *
     * @var Article
     */
    private $article;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    private $seen = true;

    /**
     * ArticleStatus constructor.
     *
     * @param User $user
     * @param Article $article
     */
    public function __construct(User $user, Article $article)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->user = $user;
        $this->article = $article;
    }

    /**
     * @return bool
     */
    public function wasSeen(): bool
    {
        return $this->seen;
    }

}
