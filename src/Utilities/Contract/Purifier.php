<?php

namespace DeepRSS\Reader\Utilities\Contract;

/**
 * Common interface for sanitizing and cleaning text content.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Purifier
{

    /**
     * @param string $content
     *
     * @return string
     */
    public function purify(string $content): string;

}