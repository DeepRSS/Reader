<?php

namespace DeepRSS\Reader\Utilities\Contract;

/**
 * Minimal storage access interface.
 *
 * Provides a base for separating domain and infrastructure layers.
 *
 * TODO: Consider whether the same interface could be applied to collections?
 * TODO: E.g. in the spirit of collections "save" could be renamed to "add"?
 *
 * TODO: Consider recommendations for implementations.
 * TODO: E.g. covariation (type safety) in getById() or should save be type safe?
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Repository
{

    /**
     * @param string $id
     *
     * @return object
     */
    public function getById(string $id);

    /**
     * @param object $entity
     *
     * @return void
     */
    public function save($entity): void;

}