<?php

namespace DeepRSS\Reader\Utilities\Contract;

/**
 * Allows to extract content from the given url.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface ContentExtractor
{

    /**
     * @param string $url
     *
     * @return string The extracted content.
     */
    public function extract(string $url): string;

}