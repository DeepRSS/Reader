<?php

namespace DeepRSS\Reader\Utilities\Contract\Exception;

/**
 * A common interface for all exceptions created by the project.
 *
 * Provides a simple way to catch all exceptions thrown by the project.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface Common extends \Throwable
{
}
