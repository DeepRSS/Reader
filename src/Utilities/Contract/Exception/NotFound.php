<?php

namespace DeepRSS\Reader\Utilities\Contract\Exception;

/**
 * Indicates that something that was expected to exists was not found.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
interface NotFound extends Common
{
}
