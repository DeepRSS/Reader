<?php

namespace DeepRSS\Reader\Utilities;

use DeepRSS\Reader\Utilities\Contract\Repository;
use Doctrine\ORM\EntityManagerInterface;
use Webmozart\Assert\Assert;

/**
 * A scaffolding for domain layer repositories based on Doctrine.
 *
 * Main added value is runtime type hinting.
 *
 * TODO: Consider trait implementation?
 * TODO: @see Repository for broder archtectural overview.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
abstract class AbstractDoctrineRepository implements Repository
{
    /**
     * @var DoctrineGateway
     */
    protected $gateway;

    /**
     * ArticleRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        // The gateway could have been injected directly, however Symfony DI can't wire that
        // EntityManager is injected in order to allow autowiring
        // More info: https://blog.fervo.se/blog/2017/07/06/doctrine-repositories-autowiring/
        /** @var DoctrineGateway $gateway */
        $gateway = $entityManager->getRepository($this->getEntityClass());
        Assert::isInstanceOf($gateway, DoctrineGateway::class);
        $this->gateway = $gateway;

    }

    /**
     * @param string $id
     *
     * @return object
     */
    public function getById(string $id)
    {
        /** @var object $entity */
        $entity = $this->gateway->find($id);

        $this->assertEntity($entity);

        return $entity;
    }

    /**
     * @param object $entity
     *
     * @return void
     */
    public function save($entity): void
    {
        $this->assertEntity($entity);

        $this->gateway->getEntityManager()->persist($entity);
    }

    /**
     * Gets an entity by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     * @param int|null $lockMode One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return object The entity instance.
     */
    protected function get($id, $lockMode = null, $lockVersion = null)
    {
        /** @var object $entity */
        $entity = $this->gateway->find($id, $lockMode, $lockVersion);

        $this->assertEntity($entity);

        return $entity;
    }

    /**
     * Gets an entity by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return object The entity instance.
     */
    protected function getOneBy(array $criteria, array $orderBy = null)
    {
        /** @var object $entity */
        $entity = $this->gateway->findOneBy($criteria, $orderBy);

        $this->assertEntity($entity);

        return $entity;
    }

    /**
     * Makes sure that given entity is instance of the expected class.
     *
     * @param mixed $entity
     *
     * @return void
     */
    protected function assertEntity($entity): void
    {
        Assert::isInstanceOf($entity, $this->getEntityClass());
    }

    /**
     * @return string
     */
    abstract protected function getEntityClass(): string;

}