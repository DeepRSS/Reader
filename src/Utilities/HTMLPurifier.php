<?php

namespace DeepRSS\Reader\Utilities;

use DeepRSS\Reader\Utilities\Contract;

/**
 * Implements HTMLPurifier based strategy of content purification.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class HTMLPurifier implements Contract\Purifier
{

    /**
     * @var \HTMLPurifier
     */
    private $purifier;

    /**
     * HTMLPurifier constructor.
     *
     * @param \HTMLPurifier $purifier
     */
    public function __construct(\HTMLPurifier $purifier)
    {
        $this->purifier = $purifier;
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function purify(string $content): string
    {
        return $this->purifier->purify($content);
    }

}