<?php

namespace DeepRSS\Reader\Utilities;

use Goose\Client;

/**
 * Provides standardized interface of content extraction based on Goose component.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class GooseContentExtractor implements Contract\ContentExtractor
{

    /**
     * @var Client
     */
    private $goose;

    /**
     * GooseContentExtractor constructor.
     *
     * @param Client $goose
     */
    public function __construct(Client $goose)
    {
        $this->goose = $goose;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function extract(string $url): string
    {
        $content = $this->goose->extractContent($url);

        if ($content instanceof \Goose\Article) {
            return (string)$content->getHtmlArticle();
        }

        return ''; // Goose could not extract anything
    }

}