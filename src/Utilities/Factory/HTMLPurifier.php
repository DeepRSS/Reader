<?php

namespace DeepRSS\Reader\Utilities\Factory;

class HTMLPurifier
{

    public static function createDefault(): \HTMLPurifier
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('CSS.MaxImgLength', null);
        $config->set('CSS.AllowedProperties', 'height');
        $config->set('HTML.Allowed', 'p,a[href],img[src|style],h1,h2,h3,h4,h5,h6,strong,br,ol,ul,li,pre');
        return new \HTMLPurifier($config);
    }

}