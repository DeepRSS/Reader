<?php

namespace DeepRSS\Reader\Utilities\Factory;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\ClientInterface as GuzzleClient;
use Zend\Feed\Reader\Http\ClientInterface as ZendClient;
use Zend\Feed\Reader\Http\Psr7ResponseDecorator;

class HTTPClient
{

    public static function createGuzzle(): GuzzleClient
    {
        /**
         * @link https://addshore.com/2015/12/guzzle-6-retry-middleware/
         * @link http://docs.guzzlephp.org/en/stable/handlers-and-middleware.html
         */
        $handlerStack = HandlerStack::create( new CurlHandler() );
        $handlerStack->push(Middleware::retry(function (
            $retries,
            RequestInterface $request,
            ResponseInterface $response = null,
            RequestException $exception = null
        ) {

            // TODO: Verify that this actually works in connection with Snapshoter::takeSnapshots()

            if ($retries > 3) {
                return false;
            }

            if ($response instanceof ResponseInterface && $response->getStatusCode() < 400) {
                return false;
            }

            if ($response instanceof ResponseInterface && $response->getStatusCode() === 404) {
                return false;
            }

            return true;

        }));

        return new \GuzzleHttp\Client([
            'timeout' => 4,
            'read_timeout' => 2,
            'connect_timeout' => 2,
            'headers' => ['User-Agent' => 'DeepRSS-dev'], // some sites (e.g. Reddit) throttle based on user agent
            'handler' => $handlerStack
        ]);
    }

    public static function createGuzzleToZendAdaptor(GuzzleClient $guzzleClient): ZendClient
    {
        return new class($guzzleClient) implements ZendClient
        {

            /**
             * @var GuzzleClient
             */
            protected $client;

            public function __construct(GuzzleClient $client)
            {
                $this->client = $client;
            }

            public function get($uri)
            {
                return new Psr7ResponseDecorator($this->client->request('GET', $uri));
            }

        };
    }

}