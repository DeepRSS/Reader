<?php

namespace DeepRSS\Reader\Utilities;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Doctrine is mixing domain and infrastructure terminology.
 * Doctrine's EntityRepository must not be confused with a domain repository.
 * Therefore, the class is renamed to a DoctrineGateway to better reflect the purpose.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class DoctrineGateway extends EntityRepository
{

    /**
     * @noinspection SenselessProxyMethodInspection
     *
     * Changes signature of findOneBy from object|null to mixed for static code analyze.
     *
     * Domain entity specific repositories are expected to provide proper type hinting.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return mixed
     *
     * @psalm-suppress LessSpecificImplementedReturnType
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return parent::getEntityManager();
    }

    public function getRepository(string $entityName): EntityRepository
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getEntityManager()->getRepository($entityName);
        return $entityRepository;
    }

}
