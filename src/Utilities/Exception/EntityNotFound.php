<?php

namespace DeepRSS\Reader\Utilities\Exception;

use DeepRSS\Reader\Utilities\Contract\Exception\NotFound;

/**
 * TODO: Add documentation.
 *
 * @author Tomasz Darmetko <tomasz.darmetko@gmail.com>
 */
class EntityNotFound extends \RuntimeException implements NotFound
{

    /**
     * @param string $entityName The entity name.
     * @param string $searchCriteria The search criteria used.
     *
     * @return EntityNotFound
     */
    public static function searchFailed(string $entityName, string $searchCriteria): self
    {
        return new static("Domain $entityName not found while searching based on criteria:\n $searchCriteria");
    }

}
